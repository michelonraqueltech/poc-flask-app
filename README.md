# Proof of Concept: Graphql client using Flask and gql3

## See the docs on `https://gql.readthedocs.io/en/stable/index.html`

## Setup

`export FLASK_APP=app.py`

`flask run`
