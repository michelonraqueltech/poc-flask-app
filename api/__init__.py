from flask import Flask
from flask_cors import CORS
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport

app = Flask(__name__)
CORS(app)

# Select your transport with a defined URL endpoint
url = "https://dropsmart-store.eu.saleor.cloud/graphql/"
localUrl = "http://localhost:8000/graphql/"

# Create an AIOHTTPTransport without headers for initial token request
transport = AIOHTTPTransport(url=url)

# Create a GraphQL client using the defined transport
client = Client(transport=transport, fetch_schema_from_transport=True)

# Provide a GraphQL query to get the token
token_query = gql(
    """
    mutation getToken($email: String!, $password: String!) {
        tokenCreate(email: $email, password: $password) {
            token
            refreshToken
            errors {
                field
                message
            }
        }
    }
"""
)

# Execute the query to get the token
params = {"email": "michelonraqueltech@gmail.com", "password": ""} 
token_result = client.execute(token_query, variable_values=params)

# Retrieve the token and the refresh token from the result
token = token_result['tokenCreate']['token']
refreshToken = token_result['tokenCreate']['refreshToken']

# Create headers with the retrieved token for subsequent requests
headers = {
    "Authorization": f"Bearer {token}"
}

# Create an AIOHTTPTransport with the URL and headers for authenticated requests
authenticated_transport = AIOHTTPTransport(url=url, headers=headers)

# Create a new GraphQL client using the authenticated transport
authenticated_client = Client(transport=authenticated_transport, fetch_schema_from_transport=True)

# Provide a GraphQL query to retrieve protected data
protected_query = gql(
    """
    query {
        me {
            id
            email
            firstName
            lastName
            isStaff
            isActive
            addresses {
            id
            firstName
            lastName
            companyName
            streetAddress1
            streetAddress2
            city
            postalCode
            country {
                code
                country
            }
            phone
            }
        }
    }
"""
)

# Execute the query on the authenticated client to retrieve protected data
whoAmI = authenticated_client.execute(protected_query)

# Provide another GraphQL query to retrieve protected data
# mutation to verify if token is valid
verify_token = gql(
    """
    mutation($token: String!) {
        tokenVerify(token: $token) {
            isValid
            errors {
                field
                code
            }
        }
    }
"""
)

# Execute the query on the authenticated client to retrieve protected data using param/variables
tokenParam = {"token": token} 
verifyToken = authenticated_client.execute(verify_token, variable_values=tokenParam)


## If received ExpiredSignatureError or to get a life longer token using the received refreshToken 
# mutation to refresh the token
refresh_token = gql(
    """
    mutation($refreshToken: String!) {
        tokenRefresh(refreshToken: $refreshToken) {
            token
        }
    }
"""
)

refreshTokenParam = {"refreshToken": refreshToken}
getNewToken = authenticated_client.execute(refresh_token, variable_values=refreshTokenParam)

newToken = getNewToken["tokenRefresh"]["token"]
tokenParam = {"token": newToken} 
verifyNewToken = authenticated_client.execute(verify_token, variable_values=tokenParam)


@app.route('/')
def hello_world():
    return str(whoAmI) + str(verifyToken) + str(getNewToken) + str(verifyNewToken)
